-module(ringbuffer).
-export([new/1]).
-export([push/2]).
-export([pop/1]).
-export([empty/1]).
-export([is_ringbuffer/1]).

new(Capacity) -> {ringbuffer,lists:duplicate(Capacity,none),0,0,Capacity,0}.
push(Ringbuffer, Value) ->
	{ringbuffer,List,ReadEnd,WriteEnd,Capacity,Stored} = Ringbuffer,
	if
		Capacity =:= Stored ->
			error;
		true ->
			{ringbuffer,
			 lists:sublist(List,WriteEnd) ++ [Value] ++ lists:nthtail(WriteEnd + 1, List),
			 ReadEnd,
			 (WriteEnd + 1) rem Capacity,
			 Capacity,
			 Stored + 1}
	end.
pop(Ringbuffer) ->
	{ringbuffer,List,ReadEnd,WriteEnd,Capacity,Stored} = Ringbuffer,
	if
		Stored =:= 0 ->
			error;
		true ->
			{lists:nth(ReadEnd + 1, List),
			 {ringbuffer,
			  List,
			  (ReadEnd + 1) rem Capacity,
			  WriteEnd,
			  Capacity,
			  Stored - 1}
			}
	end.
empty(Ringbuffer) ->
	{ringbuffer,_,_,_,_,Stored} = Ringbuffer,
	Stored =:= 0.

is_ringbuffer({ringbuffer,_,_,_,_,_}) -> true;
is_ringbuffer(_) -> false.

