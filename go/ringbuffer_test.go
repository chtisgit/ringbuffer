package ringbuffer

import "testing"
import "math/rand"
import "bytes"

const RINGBUF_LENGTH = 50

type Data struct {
	arr []byte
}

func (d *Data) fuzz(N int) {
	sz := rand.Intn(N-1)+1
	d.arr = make([]byte,sz)
	for i := 0; i < sz; i++ {
		d.arr[i] = byte(rand.Intn(256))
	}
}

func TestFuzz(t *testing.T) {
	data := []Data{}
	r := Ringbuffer{}
	r.Resize(RINGBUF_LENGTH)

	readbuf := make([]byte,RINGBUF_LENGTH)
	for N := 10000; N > 0; N-- {
		for M := rand.Intn(60); M > 0; M-- {
			d := Data{}
			d.fuzz(RINGBUF_LENGTH*2)

			if n,err := r.Write(d.arr); err == nil {
				if n == 0 {
					t.Error("Ringbuffer.Write: n == 0 and err = nil")
				}
				data = append(data, d)
			}else if len(d.arr) <= r.Free() {
				t.Error("Ringbuffer.Write fails although there is enough free space")
			}
		}

		if len(data) == 0 {
			continue
		}

		for M := rand.Intn(3); M > 0; M-- {
			a := readbuf[:len(data[0].arr)]
			if _, err := r.Read(a); err == nil {
				if bytes.Compare(a,data[0].arr) != 0{
					t.Error("Ringbuffer.Read: Ringbuffer corrupted data between write and read")
				}
				data = data[1:]
			}else{
				t.Error("Ringbuffer.Read: err != nil")
			}
		}
	}
}
