package ringbuffer

import "io"

type Ringbuffer struct {
	data []byte
	readEnd int
	writeEnd int
}

func (r *Ringbuffer) Used() int {
	return len(r.data) - r.Free()
}

func (r *Ringbuffer) Free() int {
	remain := r.readEnd - r.writeEnd
	if r.writeEnd > r.readEnd {
		remain = len(r.data) - r.writeEnd + r.readEnd - 1
	}
	return remain
}

func (r *Ringbuffer) Insert(b []byte) {
	if len(b) > len(r.data) {
		b = b[len(b) - len(r.data):]
	}

	var copied int
	if r.writeEnd > r.readEnd {
		copied = copy(r.data[r.writeEnd:],b)
	}else{
		copied = copy(r.data[r.writeEnd:r.writeEnd],b)
	}
	if copied < len(b) {
		copied += copy(r.data, b[copied:])
	}
	r.writeEnd = (r.writeEnd + copied) % len(r.data)
}

func (r *Ringbuffer) Resize(n int) {
	r.data = make([]byte,n)
}
func (r *Ringbuffer) Size() int {
	return len(r.data)
}

func (r *Ringbuffer) Write(b []byte) (int,error) {
	remain := r.Free()

	if remain < len(b) {
		return 0, io.ErrShortBuffer
	}

	r.Insert(b)
	return len(b), nil
}

func (r *Ringbuffer) Read(b []byte) (int,error) {
	var copied int

	if r.writeEnd < r.readEnd {
		copied = copy(b,r.data[r.readEnd:])
	}else{
		copied = copy(b,r.data[r.readEnd:r.writeEnd])
	}
	if copied < len(b) {
		copied += copy(b[copied:],r.data)
	}
	r.readEnd = (r.readEnd + copied) % len(r.data)
	return copied,nil
}


func (r *Ringbuffer) WritePartial(b []byte) int {
	remain := r.Free()

	if remain < len(b) {

	}

	return remain
}

